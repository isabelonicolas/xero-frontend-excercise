import * as React from "react";
import * as scss from "./NavBar.module.scss";
import xeroLogo from "../../assets/img/xero.svg";

export const NavBar = () => {
  return (
    <nav className={scss.wrapper}>
      <div className={scss.brand}>
        <a href="/" title={`Xero`}>
          <img src={xeroLogo} alt={`Xero`} />
        </a>
      </div>
    </nav>
  );
};
