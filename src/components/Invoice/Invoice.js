import * as React from "react";
import clsx from "clsx";
import { Button } from "../ui/button/button";
import * as scss from "./Invoice.module.scss";

export const Invoice = () => {
  // Local States
  const [items, setItems] = React.useState([]);
  const [total, setTotal] = React.useState(0);
  const [fields, setFields] = React.useState({});
  const [errors, setErrors] = React.useState({});

  // Effects - Update `total` when `items` updates
  React.useEffect(() => {
    if (items.length <= 0) return;

    const newTotal = items.reduce((n, { price }) => n + price, 0);

    setTotal(newTotal);
  }, [items]);

  /**
   * Handle input change
   * @param {*} evt
   */
  function handleChange(evt) {
    const newField = {};

    newField[evt.target.name] = evt.target.value;

    setFields((prevState) => {
      return {
        ...prevState,
        ...newField,
      };
    });
  }

  /**
   * Handles the form submission
   * @param {*} evt
   */
  function handleSubmit(evt) {
    evt.preventDefault();

    if (validate()) {
      const newItem = {
        description: fields.description,
        cost: parseFloat(fields.cost),
        quantity: fields.quantity,
        price: parseFloat(fields.cost * fields.quantity),
      };

      setItems((prevState) => {
        return [...prevState, newItem];
      });

      evt.target.reset();
    }
  }

  /**
   * Handles the form reset event
   * @param {*} evt
   */
  function handleReset() {
    setFields({});
  }

  /**
   * Validates 'fields'
   * @param {*} evt
   */
  function validate() {
    let isValid = true;
    let newErrors = {};

    // validate - description
    if (!fields["description"]) {
      isValid = false;

      newErrors["description"] = "This field is required.";
    }

    // validate - cost
    if (!fields["cost"]) {
      isValid = false;

      newErrors["cost"] = "This field is required.";
    } else {
      if (isNaN(fields["cost"])) {
        isValid = false;

        newErrors["cost"] = "Please enter a valid value.";
      }
    }

    // validate - quantity
    if (!fields["quantity"]) {
      isValid = false;

      newErrors["quantity"] = "This field is required.";
    } else {
      if (isNaN(fields["quantity"])) {
        isValid = false;

        newErrors["quantity"] = "Please enter a valid value.";
      }
    }

    setErrors(newErrors);

    return isValid;
  }

  /**
   * Submits the invoice summary
   */
  function submitInvoice() {
    alert("Check the console.");

    console.log("DATA: ", {
      total: total,
      items: items,
    });
  }

  /**
   * Transforms decimal
   * @param {*} num
   */
  function toUSD(num) {
    return `$${(Math.round(num * 100) / 100).toFixed(2)}`;
  }

  return (
    <div className={scss.wrapper}>
      {/* heading */}
      <h1 className={scss.heading}>Create Invoice</h1>

      {/* form */}
      <form
        className={scss.xero_form}
        method="post"
        onSubmit={(evt) => handleSubmit(evt)}
        onReset={handleReset}
      >
        <div className={scss.xero_form_fields}>
          <div
            className={clsx(
              scss.xero_form_field,
              errors.description && scss.xero_form_field_error
            )}
          >
            <label className={scss.xero_form_field_label}>Description</label>
            <div className={scss.xero_form_field_input_container}>
              <input
                type="text"
                name="description"
                onChange={(evt) => handleChange(evt)}
              />
            </div>
            {errors.description && (
              <span className={scss.xero_form_field_error_message}>
                {errors.description}
              </span>
            )}
          </div>
          <div
            className={clsx(
              scss.xero_form_field,
              errors.description && scss.xero_form_field_error
            )}
          >
            <label className={scss.xero_form_field_label}>Cost</label>
            <div className={scss.xero_form_field_input_container}>
              <input
                type="number"
                name="cost"
                step="0.01"
                onChange={(evt) => handleChange(evt)}
              />
            </div>
            {errors.cost && (
              <span className={scss.xero_form_field_error_message}>
                {errors.cost}
              </span>
            )}
          </div>
          <div
            className={clsx(
              scss.xero_form_field,
              errors.description && scss.xero_form_field_error
            )}
          >
            <label className={scss.xero_form_field_label}>Quantity</label>
            <div className={scss.xero_form_field_input_container}>
              <input name="quantity" onChange={(evt) => handleChange(evt)} />
            </div>
            {errors.quantity && (
              <span className={scss.xero_form_field_error_message}>
                {errors.quantity}
              </span>
            )}
          </div>
          <div
            className={clsx(
              scss.xero_form_field,
              scss.xero_form_field_no_label,
              errors.description && scss.xero_form_field_error
            )}
          >
            <Button
              type={`submit`}
              label={`Add item`}
              variant={`primary`}
              fullWidth={true}
            />
          </div>
        </div>
      </form>

      {/* invoice Summary */}
      <div className={scss.invoice_summary}>
        <div className={scss.invoice_summary_inner}>
          <table>
            <thead>
              <tr>
                <th>Description</th>
                <th>Cost</th>
                <th>Quantity</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {items.length > 0 ? (
                items.map((item, index) => (
                  <tr key={`item_${index}`}>
                    <td>{item.description}</td>
                    <td>{toUSD(item.cost)}</td>
                    <td>{item.quantity}</td>
                    <td>{toUSD(item.price)}</td>
                  </tr>
                ))
              ) : (
                <tr>
                  <td className="td_no_data" colSpan={4}>
                    No data available in table.
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </div>

      {/* total */}
      <div className={scss.total}>Total: {toUSD(total)}</div>

      {/* submit */}
      <div className={scss.cta_submit}>
        <Button
          label={`Submit Invoice`}
          variant={`primary`}
          disabled={items.length <= 0}
          onClick={submitInvoice}
        />
      </div>
    </div>
  );
};
