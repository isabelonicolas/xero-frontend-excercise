import * as React from "react";
import clsx from "clsx";
import * as scss from "../button/button.module.scss";

export const Button = ({
  label,
  type,
  disabled,
  variant,
  fullWidth,
  onClick,
}) => {
  function handleClick() {
    if (!onClick) return;

    onClick();
  }

  return (
    <button
      className={clsx(
        scss.btn,
        variant === "primary" && scss.btn_primary,
        variant === "success" && scss.btn_success,
        disabled && scss.btn_disabled,
        fullWidth && scss.btn_fw
      )}
      type={type}
      disabled={disabled}
      onClick={handleClick}
    >
      {label}
    </button>
  );
};
