import React, { Component } from "react";
import { Invoice } from "./components/Invoice/Invoice";
import { NavBar } from "./components/NavBar/NavBar";

class App extends Component {
  render() {
    return (
      <div>
        <header>
          <NavBar />
        </header>
        <main>
          <div className="container">
            <Invoice />
          </div>
        </main>
      </div>
    );
  }
}

export default App;
